#!/bin/bash
NOWIPADDR="/tmp/current_ip"
GETIPADDR="ifconfig.me"

CURRENT_IP=`cat $NOWIPADDR`
CHECK_IP=`curl -s $GETIPADDR`

if [[ $CURRENT_IP == $CHECK_IP ]]
then
  echo "Current IP address: $CURRENT_IP"
else
  echo "New IP address: $CHECK_IP"
  # /usr/bin/notify-send "IP address changed" $MSG
  echo $CHECK_IP > $NOWIPADDR
fi
