#!/bin/bash

# tool for finding git repositories in provided directory
# and display short summary for each repo
# usage: git-repo-list.sh [dir1 dir2 dir3 ...]

# trap 'echo ERROR AT LINE $LINENO' ERR

: ' output text colors

Num  Colour    #define         R G B

0    black     COLOR_BLACK     0,0,0
1    red       COLOR_RED       1,0,0
2    green     COLOR_GREEN     0,1,0
3    yellow    COLOR_YELLOW    1,1,0
4    blue      COLOR_BLUE      0,0,1
5    magenta   COLOR_MAGENTA   1,0,1
6    cyan      COLOR_CYAN      0,1,1
7    white     COLOR_WHITE     1,1,1
'

RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

SEARCH_DIR=$PWD

# convert to full paths
if [[ $# -gt 0 ]]; then
	SEARCH_DIR=`realpath $@`
fi

REPO_LIST=(`find $SEARCH_DIR -type d -name ".git" -print 2>/dev/null`)

if [[ ${#REPO_LIST[@]} -eq 0 ]]; then
	echo "${YELLOW}no git repositories found${RESET}"
	exit
fi

for REPO in "${REPO_LIST[@]}"; do

	REPO_DIR=`dirname $REPO}`
	REPO_DIR_ESC=$(printf %q "$REPO_DIR") # escape spaces

	echo ${YELLOW}$REPO_DIR_ESC${RESET}
	cd "$REPO_DIR"

	git branch
	git status -s

done
